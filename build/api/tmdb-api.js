"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUpcomingMovies = exports.getTopRatedMovies = exports.getSimilarMovies = exports.getPopularPeople = exports.getPopularMovies = exports.getPeopleImages = exports.getPeople = exports.getNowPlayingMovies = exports.getMovies = exports.getMovieReviews = exports.getMovieImages = exports.getMovieGenres = exports.getMovie = void 0;

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

var getNowPlayingMovies = function getNowPlayingMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/now_playing?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getNowPlayingMovies = getNowPlayingMovies;

var getTopRatedMovies = function getTopRatedMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/top_rated?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getTopRatedMovies = getTopRatedMovies;

var getPopularMovies = function getPopularMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/popular?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&include_adult=false&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getPopularMovies = getPopularMovies;

var getSimilarMovies = function getSimilarMovies(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "/similar?api_key=").concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (res) {
    return res.json();
  }).then(function (json) {
    return json.results;
  });
};

exports.getSimilarMovies = getSimilarMovies;

var getMovies = function getMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/discover/movie?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&include_adult=false&include_video=false&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getMovies = getMovies;

var getMovie = function getMovie(id) {
  // console.log(args)
  // const [, idPart] = args.queryKey;
  // const { id } = idPart;
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "?api_key=").concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getMovie = getMovie;

var getMovieImages = function getMovieImages(id) {
  // const [, idPart] = queryKey;
  // const { id } = idPart;
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "/images?api_key=").concat(process.env.TMDB_KEY)).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getMovieImages = getMovieImages;

var getMovieReviews = function getMovieReviews(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "/reviews?api_key=").concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getMovieReviews = getMovieReviews;

var getPeopleImages = function getPeopleImages(id) {
  // const [, idPart] = queryKey;
  // const { id } = idPart;
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/person/".concat(id, "/images?api_key=").concat(process.env.TMDB_KEY)).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getPeopleImages = getPeopleImages;

var getPopularPeople = function getPopularPeople() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/person/popular?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getPopularPeople = getPopularPeople;

var getPeople = function getPeople(id) {
  // const [, idPart] = args.queryKey;
  // const { id } = idPart;
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/person/".concat(id, "?api_key=").concat(process.env.TMDB_KEY)).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getPeople = getPeople;

var getMovieGenres = function getMovieGenres() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/genre/movie/list?api_key=".concat(process.env.TMDB_KEY, "&language=en-US")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getMovieGenres = getMovieGenres;

var getUpcomingMovies = function getUpcomingMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/upcoming?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getUpcomingMovies = getUpcomingMovies;